package library;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BorrowRegister {
	
	private final Book book;
	private final Date date;
	
	public BorrowRegister(Book book, Date date) {
		this.book = book;
		this.date = date;
		
	}
	
	public Book getBook() {
		return book;
	}
	
	public Date getDate() {
		return date;
	}
	
	public long getDifferenceOfDays() {
		long diff = new Date().getTime() - date.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		
	}
	
}
