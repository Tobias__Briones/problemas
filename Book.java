package library;

public class Book {
	
	private final String name;
	private boolean isForConsultancy;
	
	public Book(String name) {
		this.name = name;
		this.isForConsultancy = true;
		
	}

	public Book(String name, boolean forConsultancy) {
		this.name = name;
		this.isForConsultancy = forConsultancy;
		
	}
	
	@Override
	public String toString() {
		return name;
		
	}
	
	public void setForConsultancy(boolean forConsultancy) {
		this.isForConsultancy = forConsultancy;
		
	}
	
	public String getName() {
		return name;
		
	}
	
	public boolean isForConsultancy() {
		return isForConsultancy;
		
	}
	
}
